import React, { useState } from 'react';
import { WithDefaultLayout } from '../../components/DefautLayout';
import { Title } from '../../components/Title';
import { Page } from '../../types/Page';
import { usernameAtom } from '../../atom/usernameAtom';
import { useAtom } from 'jotai';

const IndexPage: Page = () => {
    const [username, setUsername] = useAtom(usernameAtom);
    const [usernameInput, setUsernameInput] = useState('');

    const onChange = (e) => {
        setUsernameInput(e.target.value);
    }

    const onSubmit = () => {
        setUsername(usernameInput);
    }

    return (
        <div>
            <Title name="User Name"></Title>
            <label>Current Username: {username}</label>
            <br />
            <input type="text" onChange={onChange}></input>
            <button type="button" onClick={onSubmit}>Submit Username</button>
        </div>
    );
}

IndexPage.layout = WithDefaultLayout;
export default IndexPage;
