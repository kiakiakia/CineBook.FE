import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { WithDefaultLayout } from '../../../components/DefautLayout';
import { Title } from '../../../components/Title';
import { Page } from '../../../types/Page';
import axios from 'axios';
import Joi from 'joi';
import { atom, useAtom } from 'jotai';
import { db } from '../../../localdb/dexieDb';

interface Movie {
    movieId: number,
    title: string,
    synopsis: string,
    startDate: Date | null,
    endDate: Date | null
}

export const errorMessageAtom = atom('');

const AddMoviePage: Page = () => {
    const router = useRouter();
    const [movie, setMovie] = useState<Movie>();

    useEffect(() => {
        resetMovieValue();
    }, [])

    const schema = Joi.object<Movie>({
        movieId: Joi.number().required(),
        title: Joi.string().min(6).required(),
        synopsis: Joi.string().min(10).required(),
        startDate: Joi.date().required(),
        endDate: Joi.date().greater(Joi.ref('startDate'))
    });

    const [errorMessage, setErrorMessage] = useAtom(errorMessageAtom);

    const resetMovieValue = () => {
        const tempMovie: Movie = {
            movieId: 0,
            title: '',
            synopsis: '',
            startDate: null,
            endDate: null
        };
        setMovie(tempMovie);
    }

    const onChange = (e) => {
        const fieldName = e.target.name;
        const value = e.target.value;

        const tempMovie = movie;
        if (!tempMovie) {
            return;
        }

        tempMovie[fieldName] = value;
        setMovie(tempMovie);
    }

    const onSubmit = async () => {
        if (!movie) {
            return;
        }

        const { error } = schema.validate(movie, { abortEarly: false });
        if (error) {
            console.log(error.details);
            setErrorMessage(error.message);
            return;
        }

        await db.movies.add({
            title: movie.title,
            synopsis: movie.synopsis,
            startDate: movie.startDate,
            endDate: movie.endDate
        });

        await axios.post('/api/demo/api/Movie/add-movie', movie)
            .then(() => {
                router.push('/movies');
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const [moviesDexie, setMoviesDexie] = useState<Movie[]>();
    const [showDexieDb, setShowDexieDb] = useState<boolean>(false);
    const loadDexieDb = async () => {
        const dbResult = await db.movies.toArray();
        const tempMovies: Movie[] = [];
        dbResult.forEach((movie) => {
            if (!movie.movieId) {
                return;
            }

            const tempMovie: Movie = {
                movieId: movie.movieId,
                title: movie.title,
                synopsis: movie.synopsis,
                startDate: movie.startDate,
                endDate: movie.endDate
            };

            tempMovies.push(tempMovie);
        });
        setMoviesDexie(tempMovies);
        setShowDexieDb(true);
    }

    const generateMovieDexie = () => {
        if (!showDexieDb) {
            return <></>;
        }

        if (!moviesDexie) {
            return <></>;
        }
        return (
            <table>
                <thead>
                    <tr>
                        <th>Movie ID</th>
                        <th>Title</th>
                        <th>Synopsis</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                </thead>
                <tbody>
                    {moviesDexie.map((movieDexie) => {
                        return (
                            <tr key={`row_dexie_${movieDexie.movieId}`}>
                                <td>{movieDexie.movieId}</td>
                                <td>{movieDexie.title}</td>
                                <td>{movieDexie.synopsis}</td>
                                <td>{movieDexie.startDate}</td>
                                <td>{movieDexie.endDate}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }

    return (
        <div>
            <Title name="Add Movie"></Title>
            <h1>This is add movie page</h1>
            <form>
                <div>
                    <label>Title: </label>
                    <input type="text" name="title" onChange={onChange}></input>
                </div>
                <div>
                    <label>Synopsis: </label>
                    <input type="text" name="synopsis" onChange={onChange}></input>
                </div>
                <div>
                    <label>Start Date: </label>
                    <input type="date" name="startDate" onChange={onChange}></input>
                </div>
                <div>
                    <label>End Date: </label>
                    <input type="date" name="endDate" onChange={onChange}></input>
                </div>
                <button type="button" onClick={onSubmit}>Submit</button>
                {errorMessage ? <ErrorMessage /> : null}
            </form>
            <br />
            <button type="button" onClick={loadDexieDb}>Load Dexie DB</button>
            {generateMovieDexie()}
        </div>
    );
}

const ErrorMessage: React.FC = () => {
    const [errorMessage] = useAtom(errorMessageAtom);

    return (
        <span><strong>{errorMessage}</strong></span>
    )
}

AddMoviePage.layout = WithDefaultLayout;
export default AddMoviePage;